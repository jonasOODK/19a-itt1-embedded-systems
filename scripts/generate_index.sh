#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <filelist or wildcard>"
	exit 1
fi

COMMIT_DATE=${@:1:3}

echo Commit time is : $COMMIT_TIME > /dev/stderr

cat << EndOfMessage
<!DOCTYPE html>
<html lang="en">
<head>
  	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title>19a-itt1-embedded-systems files</title>
</head>
<body>
	<div class="container-fluid text-center">
        <div class="row">
            <h1 class="col-sm-12">19A-ITT1-EMBEDDED-SYSTEMS</h1>
        </div>
        <div class="row">Below you can find files related to the UCL IT-Technology 1st semester embedded systems classes<br> These are updated regularly so please update your offline versions often</div>
        <br>
	    <div class="btn-group-vertical text-center">
            <a class="btn btn-success btn-lg" target="_blank" href="19A_ITT1_embedded_lecture_plan.pdf">lecture plan (PDF)</a>
            <a class="btn btn-success btn-lg" target="_blank" href="19A_ITT1_embedded_weekly_plans.pdf">weekly plans (PDF)</a>
            <a class="btn btn-success btn-lg" target="_blank" href="weekly_plans">weekly plans (MD)</a>
	    </div>
	    <div class="col-sm-12">generated @ $COMMIT_TIME</div>
    </div>
</body>
</html>
EndOfMessage
