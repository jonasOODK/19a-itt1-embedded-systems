---
Week: 38
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 38 ITT1 Embedded Systems

## Goals of the week(s)
Students will be introduced to signals, analog and digital, focus on Pulse-width modulation (PWM) as a digital control signal.

### Practical goals
1. Create a simple ON/OFF (Blink) LED control software controlled by PWM.
2. Update the existing circuit to veroboard. Add buttons and connect them as inputs.
3. Create a menu, blink or answer questions.

### Learning goals
1. Various signal types.
2. Pulse width modulation.
3. Controlling with signal.

## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Links

In this section links, tutorials and other useful starting points will be posted.

### Day 2 Week 38

https://www.youtube.com/watch?v=9tActipVqIM

https://www.youtube.com/watch?v=Zjr69DxgeW0

https://www.elprocus.com/pull-up-and-pull-down-resistors-with-applications/

https://www.chegg.com/homework-help/definitions/analog-signal-4

https://www.youtube.com/watch?v=8JLM65YwxS8

https://www.youtube.com/watch?v=F5h3z8p9dPg

https://www.chegg.com/homework-help/definitions/digital-signal-4

https://www.youtube.com/watch?v=jRL9ag3riJY

https://www.youtube.com/watch?v=RQc_frX50BA

## Comments
N/A
