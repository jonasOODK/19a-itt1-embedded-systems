---
Week: 39
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 39 ITT1 Embedded Systems

## Goals of the week(s)
Students will be introduced to signals, analog and digital, focus on signal conversion.

### Practical goals

1. Students will implement a analog to digital conversion simple system. Week `39`
2. Students will measure a voltage power-supply and read them into the raspberry pi. Week `39`
3. Students will measure the voltage from a battery pack - power bank. Week `40`

### Learning goals

1. Analog to Digital signal conversion.
2. The MCP3008.


## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Links

In this section links, tutorials and other useful starting points will be posted.

### Guides

A quick guide to set up RPi with static IP via Ethernet cable (local network/LAN), by using eth0.

https://www.ionos.com/digitalguide/server/configuration/provide-raspberry-pi-with-a-static-ip-address/

pay attention that you may get issues with the subnet (aka /24), and the missing static domain.
You may need, `may`need to add `static domain_search=`

My configuration are: 

\begin{verbatim}
interface eth0

static ip_address=192.168.0.10/24 #replace with yours, 
and be aware when you move to another computer that the ssh key will give you a warning

static routers=192.168.0.1

static domain_name_servers=192.168.0.1

static domain_search=

\end{verbatim}

remember to edit your local network settings and assign a static IP to your network adapter.


### Day 3 Week 39 - week 40

same links and material will be used in week 40 as well.


https://www.sciencedirect.com/topics/engineering/analog-to-digital-converter

https://www.arrow.com/en/research-and-events/articles/engineering-resource-basics-of-analog-to-digital-converters

https://www.elprocus.com/analog-to-digital-adc-converter/

https://tutorial.cytron.io/2018/11/14/reading-analog-signal-using-mcp3008-raspberry-pi/

https://pimylifeup.com/raspberry-pi-adc/
https://www.instructables.com/id/ADC-MCP3008-Raspberry-Pi/

https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/mcp3008

https://electronicshobbyists.com/raspberry-pi-analog-sensing-mcp3008-raspberry-pi-interfacing/

https://www.youtube.com/watch?v=QHlWV904jNw

https://www.youtube.com/watch?v=jDJAklvIuPY


## Comments
N/A
