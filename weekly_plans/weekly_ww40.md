---
Week: 40
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 40 ITT1 Embedded Systems

## Goals of the week(s)
Continuation from week `39`. Week `39` links are also valid in this week 

### Practical goals

1. Students will implement a analog to digital conversion simple system. Week `39`
2. Students will measure a voltage power-supply and read them into the raspberry pi. Week `39`
3. Students will measure the voltage from a battery pack - power bank. Week `40`

### Learning goals

1. Analog to Digital signal conversion.
2. The MCP3008.


## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverables.

## Comments
N/A
