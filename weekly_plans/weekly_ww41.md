---
Week: 41
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 41 ITT1 Embedded Systems

## Goals of the week(s)
Introduction to BASH. Students will be presented the linux OS and the BASH command line and script options.

### Practical goals

1. The students will write a simple BASH program that will start their python scrip. Start on, start up.
2. BASH will control and copy the python script.
3. BASH will check status of CPU load, Memory, and Disk-space

### Learning goals

1. BASH scripting.
2. Using BASH identify system parameters and peripheral.


## Deliverable

1. Present output of commands and document with screen shoots.
2. This is a mandatory deliverable via wiseflow.

## Schedule

See Time Edit

## Hands-on time

This hand-in is mandatory and it is done in wiseflow.

## Comments
N/A
