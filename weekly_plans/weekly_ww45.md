---
Week: 45
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 45 ITT1 Embedded Systems

## Goals of the week(s)
Introduction of DC and AC motors. The students will be presented with the theory behind DC motors and H-Bridges. A simple introduction to AC motors.

### Practical goals

1. Students will link DC motors to the RPI and control them via PWM, with the help of an H-Bridge.
2. Students will be interface buttons and control direction of motor.

### Learning goals

1. Simple Motor Theory.
2. Simple H-Bridge and control theory.


## Deliverable

1. Hand in Gitlab, software.
2. Hand in Gitlab, oscilloscope output, from buttons and DC Motor interface.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
