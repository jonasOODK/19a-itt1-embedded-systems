---
Week: 46
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 46 ITT1 Embedded Systems

## Goals of the week(s)
Continue work from Week `45`

### Practical goals

1. Students will link DC motors to the RPI and control them via PWM
2. Students will be interface buttons and control direction of motor.

### Learning goals

1. Implement of motor driver.
2. Implement simple motor control.


## Deliverable

1. Hand in Gitlab, software.
2. Hand in Gitlab, oscilloscope output, from buttons and DC Motor interface.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
