---
Week: 48
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 48 ITT1 Embedded Systems

## Goals of the week(s)
Project Work. An simple embedded project that connects buttons, a sensor, a joystick and an actuator will run for the rest of the semester.

### Practical goals

1. Create a motor control software with PWM via an H-bridge.
2. Interface buttons to start and stop the motor.
3. Interface buttons to start and stop the software.

### Learning goals

1. Interfacing sensors, actuators, analog input and digital inputs.



## Deliverable

1. Motor and H-bridge diagram.
2. Software.
3. Button interface diagrams.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
