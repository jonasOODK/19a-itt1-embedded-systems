---
Week: 02
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 02 ITT1 Embedded Systems

## Goals of the week(s)
Poster Presentation

### Practical goals

1. Poster Presentation

### Learning goals

1. Present a Poster



## Deliverable

1. A posters needs to be handed in in Gitlab

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
